import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class Product {
  Product(this.object);

  final Map object;

  int get id => object['id'];

  String get title => object['title'];

  double get price {
    final _price = object['price'];
    if (_price is double) {
      return _price;
    } else if (_price is int) {
      return _price.toDouble();
    }

    return 0;
  }

  String get category => object['category'];

  String get description => object['description'];

  String get image => object['image'];

}
