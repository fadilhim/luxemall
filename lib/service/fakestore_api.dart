import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:luxemall/model/product.dart';

class APIConfig {
  String productUrl(String query) {
    return 'http://fakestoreapi.com/products$query';
  }

  String cartUrl(String query) {
    return 'http://fakestoreapi.com/carts$query';
  }
}

// TODO: next limit fetch
class API {
  static int limit = 4;
  static String userId = '5';

  static getAllCategories() async {
    final _url = APIConfig().productUrl('/categories');

    final response = await http.get(_url);

    final List _body = json.decode(response.body) ?? null;

    return _body;
  }

  static Future<List<Product>> getAllProduct({Sort sort}) async {
    String _url = APIConfig().productUrl('');

    if (sort == Sort.ascending) {
      _url += '?sort=asc';
    } else if (sort == Sort.descending) {
      _url += '?sort=desc';
    }

    final response = await http.get(_url);

    final List _body = json.decode(response.body);

    return List.generate(_body.length, (index) => Product(_body[index]));
  }

  static Future<List<Product>> getProductByCategory(
      {@required String category, Sort sort}) async {
    String _url = APIConfig().productUrl('/category/$category');

    if (sort == Sort.ascending) {
      _url += '?sort=asc';
    } else if (sort == Sort.descending) {
      _url += '?sort=desc';
    }

    final response = await http.get(_url);

    final List _body = json.decode(response.body) ?? null;

    return List.generate(_body.length, (index) => Product(_body[index]));
  }

  static Future<Product> getSingleProduct({@required int id}) async {
    final _url = APIConfig().productUrl('/$id');

    final response = await http.get(_url);

    final Map _body = json.decode(response.body) ?? null;

    return Product(_body);
  }

  static addProductToCart(
      {@required Product product, @required int total}) async {
    final _url = APIConfig().cartUrl('');
    final _bodyUrl = {
      'userId': userId,
      'date': DateTime.now().millisecondsSinceEpoch,
      'products': [
        {
          'productId': product.id,
          'quantity': total,
        }
      ]
    };

    final response = await http.post(_url, body: json.encode(_bodyUrl));

    final Map _body = json.decode(response.body) ?? null;

    return Product(_body);
  }
}

enum Sort { ascending, descending }
