import 'package:flutter/material.dart';

class ThemeColors {
  static const int _goldPrimaryValue = 0xFFC2912E;
  static const MaterialColor gold = MaterialColor(
    _goldPrimaryValue,
    <int, Color>{
      50: Color.fromRGBO(194, 145, 46, .1),
      100:  Color.fromRGBO(194, 145, 46, .2),
      200:  Color.fromRGBO(194, 145, 46, .3),
      300:  Color.fromRGBO(194, 145, 46, .4),
      400:  Color.fromRGBO(194, 145, 46, .5),
      500:  Color(_goldPrimaryValue),
      600:  Color.fromRGBO(194, 145, 46, .7),
      700:  Color.fromRGBO(194, 145, 46, .8),
      800:  Color.fromRGBO(194, 145, 46, .9),
      900:  Color.fromRGBO(194, 145, 46, 1),
    },
  );

  static const int _greyPrimaryValue = 0xFF585858;
  static const MaterialColor grey = MaterialColor(
    _greyPrimaryValue,
    <int, Color>{
      50: Color.fromRGBO(88, 88, 88, .1),
      100:  Color.fromRGBO(88, 88, 88, .2),
      200:  Color.fromRGBO(88, 88, 88, .3),
      300:  Color.fromRGBO(88, 88, 88, .4),
      400:  Color.fromRGBO(88, 88, 88, .5),
      500:  Color(_greyPrimaryValue),
      600:  Color.fromRGBO(88, 88, 88, .7),
      700:  Color.fromRGBO(88, 88, 88, .8),
      800:  Color.fromRGBO(88, 88, 88, .9),
      900:  Color.fromRGBO(88, 88, 88, 1),
    },
  );

}
