import 'package:flutter/material.dart';

import 'package:luxemall/theme/theme_colors.dart';
import 'view/splash_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Luxemall',
      theme: ThemeData(
        primarySwatch: ThemeColors.gold,
        accentColor: ThemeColors.grey,
      ),
      home: SplashScreen(),
    );
  }
}
