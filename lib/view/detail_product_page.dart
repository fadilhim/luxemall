import 'package:flutter/material.dart';

import 'package:luxemall/model/product.dart';
import 'package:luxemall/service/fakestore_api.dart';

class DetailProductPage extends StatefulWidget {
  Product product;

  DetailProductPage({Key key, this.product}) : super(key: key);

  @override
  _DetailProductPageState createState() => _DetailProductPageState();
}

class _DetailProductPageState extends State<DetailProductPage> {
  bool initFetch = true;
  bool fetching = false;
  String errorMessage;
  Product product;
  int total = 0;

  @override
  void initState() {
    fetch();
    super.initState();
  }

  // fetch product detail
  fetch() async {
    try {
      setState(() {
        if (widget.product != null) {
          product = widget.product;
        }
      });
      print(product.object);
    } catch (e) {
      setState(() {
        errorMessage = 'error load data';
      });
    }

    setState(() {
      initFetch = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).accentColor,
        bottomNavigationBar: bottomBar,
        body: Hero(
            tag: widget.product.id,
            child: SingleChildScrollView(
              child: Stack(
                children: [
                  Builder(builder: (context) {
                    if (initFetch) {
                      return Center(child: CircularProgressIndicator());
                    }
                    return body;
                  }),
                  Positioned(child: appBar)
                ],
              ),
            )));
  }

  Widget get appBar {
    return AppBar(
      iconTheme: IconThemeData(
        color: Theme.of(context).primaryColor,
      ),
      backgroundColor: Colors.transparent,
      elevation: 0.0,
    );
  }

  Widget get body {
    return RefreshIndicator(
        onRefresh: () => fetch(),
        child: Column(children: [
          image,
          title,
          SizedBox(height: 5),
          description,
          SizedBox(height: 5),
          totalSection
        ]));
  }

  Widget get image => Container(
      padding: EdgeInsets.only(top: 30),
      width: double.infinity,
      color: Colors.white,
      child: Center(
          child:
              Image.network(product.image, fit: BoxFit.contain, height: 300)));

  Widget get title {
    final titleStyle = Theme.of(context)
        .textTheme
        .headline6
        .copyWith(fontWeight: FontWeight.bold);

    return Container(
        padding: EdgeInsets.only(left: 12.5, right: 12.5, bottom: 10, top: 20),
        width: double.infinity,
        color: Colors.white,
        child: Column(children: [
          Text(product.title,
              style:
                  titleStyle.copyWith(color: Theme.of(context).primaryColor, fontSize: 17), textAlign: TextAlign.center),
        ]));
  }

  Widget get description {
    final titleStyle = Theme.of(context)
        .textTheme
        .headline6
        .copyWith(fontWeight: FontWeight.bold);
    final descriptionStyle = Theme.of(context).textTheme.bodyText1;

    return Container(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 12.5),
        width: double.infinity,
        color: Colors.white,
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text('Description', style: titleStyle),
          SizedBox(height: 15),
          Text(product.description,
              style: descriptionStyle, textAlign: TextAlign.justify),
        ]));
  }

  Widget get totalSection {
    final titleStyle = Theme.of(context)
        .textTheme
        .headline6
        .copyWith(fontWeight: FontWeight.bold);

    return Container(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 12.5),
        width: double.infinity,
        color: Colors.white,
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text('Total', style: titleStyle),
          SizedBox(height: 15),
          Center(child: buttonAddRemove)
        ]));
  }

  Widget get buttonAddRemove => Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          FlatButton(
            child: Icon(Icons.remove),
            onPressed: removeItem,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            color: Theme.of(context).primaryColor,
            textColor: Colors.white,
          ),
          SizedBox(width: 8),
          Text(total.toString(), style: TextStyle(fontWeight: FontWeight.bold)),
          SizedBox(width: 8),
          FlatButton(
            child: Icon(Icons.add),
            onPressed: addItem,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            color: Theme.of(context).primaryColor,
            textColor: Colors.white,
          ),
        ],
      );

  Widget get bottomBar => Container(
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                blurRadius: 5.0,
                color: Colors.black12.withOpacity(0.1),
                spreadRadius: 2.0)
          ],
        ),
        padding: EdgeInsets.all(7.5),
        child: !fetching
            ? FlatButton(
                color: Theme.of(context).primaryColor,
                textColor: Colors.white,
                disabledColor: Colors.grey,
                disabledTextColor: Colors.black,
                padding: EdgeInsets.all(5.0),
                splashColor: Colors.blueAccent,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0)),
                onPressed: () => handleCheckout(),
                child: Text('Checkout'),
              )
            : OutlineButton.icon(
                label: Text('Loading...'),
                icon: CircularProgressIndicator(),
                // TODO: themecolors
                color: Colors.green,
                textColor: Colors.white,
                onPressed: null,
              ),
      );

  handleCheckout() async {
    if (total <= 0)
      return showSnackBar('Please add product to your cart first');
    final res = await API.addProductToCart(product: product, total: total);

    showSnackBar('Add $total ${product.title} to your cart');
    Navigator.pop(context);
  }

  addItem() {
    setState(() {
      total++;
    });
  }

  removeItem() {
    if (total > 0) {
      setState(() {
        total--;
      });
    }
  }

  showSnackBar(String message) {
    ScaffoldMessenger.of(context)
        .showSnackBar(SnackBar(content: Text(message)));
  }
}
