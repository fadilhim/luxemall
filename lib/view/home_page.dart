import 'package:flutter/material.dart';

import 'package:luxemall/model/product.dart';
import 'package:luxemall/service/fakestore_api.dart';
import 'package:luxemall/view/detail_product_page.dart';

class HomePage extends StatefulWidget {
  List categories;

  HomePage({Key key, this.categories}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool initFetch = true;
  bool fetching = false;
  String errorMessage;
  List categories = [];
  String category;
  List<Product> products = [];
  String alphabetSort = 'asc';
  int indexPage = 1;
  int limitPage = 4;

  @override
  void initState() {
    fetch();
    super.initState();
  }

  // fetch categories and all products
  fetch() async {
    try {
      final _products = await API.getAllProduct();

      setState(() {
        if (widget.categories != null) {
          categories = widget.categories;
        }
        products = _products;
      });
    } catch (e) {
      setState(() {
        errorMessage = 'error load data';
      });
    }

    setState(() {
      initFetch = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar,
      drawer: drawer,
      body: Builder(builder: (context) {
        if (initFetch) {
          return Center(child: CircularProgressIndicator());
        }
        return body;
      }),
    );
  }

  Widget get appBar {
    return AppBar(
      backgroundColor: Theme.of(context).primaryColor,
      title: Text('LuxeMall'),
      centerTitle: true,
      actions: [
        Padding(
            padding: EdgeInsets.only(right: 7.5),
            child: Icon(Icons.shopping_bag)),
        Padding(
            padding: EdgeInsets.only(right: 7.5), child: Icon(Icons.person)),
        Padding(
            padding: EdgeInsets.only(right: 15.0),
            child: Icon(Icons.notifications))
      ],
    );
  }

  Widget get drawer {
    return Drawer(
      child: ListView(
        children: <Widget>[
          ListTile(title: Text('Sale'.toUpperCase())),
          ListTile(title: Text('Designers'.toUpperCase())),
        ],
      ),
    );
  }

  Widget get body {
    final titleStyle = Theme.of(context).textTheme.headline6;

    return RefreshIndicator(
        onRefresh: () => fetch(),
        child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 12.5, vertical: 10),
            child: Column(children: [
              Text(
                  category != null
                      ? category.toUpperCase()
                      : 'All'.toUpperCase(),
                  style: titleStyle.copyWith(
                      fontSize: 20, color: Theme.of(context).primaryColor)),
              SizedBox(height: 15),
              Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [categorySortField, alphabeticallySortField]),
              SizedBox(height: 10),
              Expanded(child: Builder(builder: (context) {
                if (fetching) {
                  return Center(child: CircularProgressIndicator());
                }

                List<Widget> children = <Widget>[];
                bool lastPage = indexPage * limitPage > products.length;
                int ind = lastPage ? products.length : indexPage * limitPage;
                for (int i = 0; i < ind; i++) {
                  children.add(itemCard(products[i]));
                }

                return NotificationListener<ScrollNotification>(
                    onNotification: (scrollNotification) {
                      if (scrollNotification.metrics.pixels ==
                          scrollNotification.metrics.maxScrollExtent) {
                        print(indexPage * limitPage < products.length);
                        if (indexPage * limitPage < products.length) {
                          setState(() {
                            indexPage++;
                          });
                        }
                      }
                    },
                    child: GridView.count(
                        childAspectRatio: 0.8,
                        crossAxisCount: 2,
                        children: children));
              }))
            ])));
  }

  Widget itemCard(Product product) {
    final titleStyle = Theme.of(context).textTheme.headline6;

    return GestureDetector(
        onTap: () => navigateToDetailPage(product),
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Hero(
                  tag: product.id,
                  child: Image.network(product.image,
                      fit: BoxFit.contain, height: 150, width: 100)),
              SizedBox(height: 10),
              Text(
                product.title,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 7.5),
              Text('\$' + product.price.toString(),
                  style: titleStyle.copyWith(
                      color: Theme.of(context).primaryColor, fontSize: 15)),
            ],
          ),
        ));
  }

  Widget get categorySortField {
    return OutlineButton(
      onPressed: () => showFilterModal(),
      child:
          Row(children: [Icon(Icons.tune), SizedBox(width: 5), Text('Filter')]),
    );
  }

  Widget get alphabeticallySortField {
    return DropdownButton<String>(
      hint: Text(alphabetSort),
      value: alphabetSort,
      items: [
        DropdownMenuItem<String>(
          value: 'asc',
          child: new Text('A to Z'),
        ),
        DropdownMenuItem<String>(
          value: 'desc',
          child: new Text('Z to A'),
        )
      ],
      onChanged: (value) => sortByAlphabet(value),
    );
  }

  showFilterModal() async {
    final titleStyle = Theme.of(context).textTheme.headline6;

    List<Widget> categoriesChild = <Widget>[];
    categories.forEach((_category) {
      if (_category == category) {
        categoriesChild.addAll([
          FlatButton(
            color: Theme.of(context).primaryColor,
            child: Text(_category, style: TextStyle(color: Colors.white)),
            onPressed: () => Navigator.pop(context, _category),
          ),
          SizedBox(width: 5)
        ]);

        return;
      }
      categoriesChild.addAll([
        OutlineButton(
          child: Text(_category),
          onPressed: () => Navigator.pop(context, _category),
        ),
        SizedBox(width: 5)
      ]);
    });

    List<Widget> children = <Widget>[
      Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Text('Filter',
            style:
                titleStyle.copyWith(fontWeight: FontWeight.bold, fontSize: 17)),
        GestureDetector(
          child: Text('Reset',
              style: titleStyle.copyWith(
                  fontWeight: FontWeight.bold,
                  fontSize: 14,
                  color: Theme.of(context).primaryColor)),
          onTap: () => resetFilter(),
        )
      ]),
      SizedBox(height: 10),
      Text('Category',
          style:
              titleStyle.copyWith(fontWeight: FontWeight.bold, fontSize: 14)),
      SizedBox(height: 7.5),
      Wrap(
        children: categoriesChild,
      )
    ];

    final res = await showModalBottomSheet(
        context: context,
        builder: (builder) {
          return Container(
            height: 175.0,
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: const Radius.circular(50.0),
                    topRight: const Radius.circular(30.0))),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: children),
          );
        });

    if (res is String) {
      sortByCategory(res);
    }
  }

  sortByCategory(String _category) async {
    setState(() {
      indexPage = 1;
      fetching = true;
    });

    List<Product> res = [];
    res = await API.getProductByCategory(
        category: _category,
        sort: alphabetSort == 'desc' ? Sort.descending : Sort.ascending);

    setState(() {
      products = res;
      category = _category;
      fetching = false;
    });
  }

  sortByAlphabet(String sort) async {
    setState(() {
      indexPage = 1;
      fetching = true;
    });

    List<Product> res = [];
    if (category == null) {
      res = await API.getAllProduct(
          sort: sort == 'desc' ? Sort.descending : Sort.ascending);
    } else {
      res = await API.getProductByCategory(
          category: category,
          sort: sort == 'desc' ? Sort.descending : Sort.ascending);
    }

    setState(() {
      products = res;
      alphabetSort = sort;
      fetching = false;
    });
  }

  resetFilter() async {
    final _products = await API.getAllProduct();

    setState(() {
      indexPage = 1;
      alphabetSort = 'asc';
      category = null;
      products = _products;
    });

    Navigator.pop(context);
  }

  navigateToDetailPage(Product product) async {
    final _products = await API.getSingleProduct(id: product.id);

    Navigator.of(context).push(MaterialPageRoute(
        builder: (BuildContext context) => new DetailProductPage(product: _products)));
  }
}
