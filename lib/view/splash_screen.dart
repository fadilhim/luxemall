import 'dart:async';

import 'package:flutter/material.dart';

import 'package:luxemall/view/home_page.dart';
import 'package:luxemall/service/fakestore_api.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  bool welcome = false;

  navigator(Widget page) {
    Navigator.of(context).pushReplacement(PageRouteBuilder(
        pageBuilder: (_, __, ___) => page,
        transitionDuration: Duration(milliseconds: 2000),
        transitionsBuilder: (_, Animation<double> animation, __, Widget child) {
          return Opacity(
            opacity: animation.value,
            child: child,
          );
        }));
  }

  // Fetch all categories
  _fetchAllCategories() async {
    final categories = await API.getAllCategories();

    return Timer(Duration(milliseconds: 200),
        () => navigator(HomePage(categories: categories)));
  }

  @override
  void initState() {
    super.initState();
    _fetchAllCategories();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: Builder(builder: (context) => screen),
    );
  }

  Widget get screen => Container(
        color: Colors.white,
        child: Center(
          child: Padding(
            padding: const EdgeInsets.only(bottom: 60.0),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Welcome To".toUpperCase(),
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 20.0,
                        fontWeight: FontWeight.w700),
                  ),
                  Text(
                    "Luxemall",
                    style: TextStyle(
                        color: Theme.of(context).primaryColor,
                        fontSize: 44.0,
                        fontWeight: FontWeight.w700),
                  )
                ]),
          ),
        ),
      );
}
